install-app:
	docker-compose exec -uwebserver php composer install

update-app:
	docker-compose exec -uwebserver php composer update

build:
	docker-compose up -d --build

shutdown:
	docker-compose down