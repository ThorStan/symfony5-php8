***Use this repo on your own risk! You have been warned.***

To start just checkout/clone this repo. Make sure docker is installed and running. Open a terminal, change to the repo directory
and execute following commands to build the container and install symfony via composer.

```/bin/bash
> /Projekte/Docker/symfony5$ make build
> /Projekte/Docker/symfony5$ make install-app
```

The database configuration must be installed and configured by yourself ;-) 

If everything did work and the containers are up and running, the application is reachable under "http://symfony5.local".
Phpmyadmin is reachable under "http://localhost:8000". For "Server" enter "mysql". Username/Password are both "root".